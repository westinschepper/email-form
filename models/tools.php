<?
	class tools {
	
		public function getView($file = '', $data = array()){
			include $file;
		
		}
		
		public function sendEmail($userName, $userEmail, $userSubject, $userMessage){
			require_once("PHPMailer/class.phpmailer.php"); //include the PHPMailer class file
		    require_once("PHPMailer/class.smtp.php"); //include the SMTP class file
					
		    $toName = "Your Name"; //Enter your name here. example: "John Doe"
		    $to = "youremail@domain.com"; //What email should this go to?
					
		    $mail = new PHPMailer(); //Initiate PHPMailer			
		    $mail->IsSMTP(); //Tells PHPMailer were using SMTP
		    $mail->SMTPAuth = true; //Turn on Authorization
		    $mail->SMTPSecure = "tls"; //Set the Encryption mode ("ssl", or "tls")
		    $mail->Host = "your.host.com"; //The URL that hosts your email server
		    $mail->Port = 587; //Your email server port number. Common: 465 or 587
		    $mail->Username = "youremail@domain.com"; //Your email
		    $mail->Password = "fluffybunnies"; //Password associated with your email
		    $mail->SetFrom($userEmail, $userName); //Sets up who the email is from
		    $mail->AddReplyTo($userEmail, $userName); //Sets up who the email replies to
		    $mail->AddAddress($to, $toName); //Sets up the email to send to
		    $mail->Subject = $userSubject; //Sets the email subject
		    $mail->Body = wordwrap($userMessage, 70); //Sets the email message
		    $result = $mail->Send(); //Sends the email
		
		    return $result; //Returns true if the email sent, false if it failed
			
		}
		
	}
	
?>