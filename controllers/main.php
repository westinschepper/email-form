<?
	
include 'models/tools.php';
$tools = new tools();

	if (!empty($_GET["action"])) { //If our action is not empty
		
		if ($_GET["action"] == "sendEmail") { //If our action is equal to sendMail
			$name = $_POST["name"]; //Grab the name field
			$email = $_POST["email"]; //Grab the email field
			$subject = $_POST["subject"]; //Grab the subject field
			$message= $_POST["message"]; //Grab the message field
			
			$result = $tools->sendEmail($name, $email, $subject, $message); //Result is equal to either true or false
			
			if ($result) { //If the email sent
				
				$tools->getView('views/success.php'); //Load a success message for the user
				
			} else { //If the email failed to send
				
				$tools->getView('views/failed.php'); //Load a failure message for the user
				
			}
			
		}
		
	} else { //If our action is empty
		
		$tools->getView('views/contact.php'); //Load the contact form
		
	}

?>