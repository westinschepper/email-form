<div class="row">
	<h1 class="text-center">Contact Us</h1>
	<form class="medium-6 medium-centered columns" action="?action=sendEmail" method="post">
		<input type="text" name="name" placeholder="Full Name">
		<input type="email" name="email" placeholder="Email">
		<input type="text" name="subject" placeholder="Subject">
		<textarea name="message" rows="10" cols="50" placeholder="Enter your message..."></textarea>
		<input class="button expand" type="submit">
	</form>
</div>