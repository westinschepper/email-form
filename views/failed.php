<div class="row">
	<h1 class="text-center">Failed</h1>
	<p class="text-center">Something went wrong while sending your message, please try again.</p>
	<a class="button" href="?" class="text-center">Go Back</a>
</div>